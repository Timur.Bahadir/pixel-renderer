# PixelRenderer

A tiny library to render pixel maps using SDL2.

## Building

Requirements:

- Installed CMake version 3.5
- Installed SDL2 library (booth development and runtime)

To build using CMake run:

```bash
mkdir build
cd build
cmake ..
cmake --build .
```

## Usage

Consume the library in your cmake projekt and link it.

```cmake
find_project(SDL2 REQUIRED)

add_subdirectory(lib/PixelRenderer)

target_link_libraries(cool_project
                      PUBLIC pr::pixelrenderer
                             SDL2::SDL2main
                             SDL2::SDL2)
```

Then use the pr::PixelRenderer class.

```c++
// Create a PixelRenderer
pr::PixelRenderer renderer{pr::WindowSettings{"Window Name",
    window_width, window_height,
    texture_width, texture_height}};

// Change a pixel
renderer->change_pixel(x, y, 0xFFE74C3C);

// Draw the texture
renderer->draw();
```

The created texture gets stretched to fit the created window.
