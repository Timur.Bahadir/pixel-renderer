#pragma once

#include <string>

namespace pr {
using size_t = uint16_t;

struct WindowSettings {
  WindowSettings(std::string const &wn, size_t const ww, size_t const wh,
                 size_t const tw, size_t const th)
      : window_name{wn}, window_width{ww}, window_height{wh}, texture_width{tw},
        texture_height{th} {}

  std::string const window_name;

  size_t const window_width;
  size_t const window_height;

  size_t const texture_width;
  size_t const texture_height;
};

class PixelRenderer {
public:
  using pixel_t = uint32_t;

  virtual ~PixelRenderer() = default;

  virtual void change_pixel(size_t const x, size_t const y,
                            pixel_t const px) = 0;

  virtual void draw() = 0;
};

} // namespace pr
