#pragma once

#include "PixelRenderer/PixelRenderer.hpp"

#include <SDL2/SDL.h>

#include <memory>

namespace pr {

class SDL_PixelRenderer final : public PixelRenderer {
public:
  SDL_PixelRenderer(WindowSettings const &ws);
  ~SDL_PixelRenderer();

  void change_pixel(size_t const x, size_t const y, pixel_t const px) override;

  void draw() override;

  SDL_Texture *sdl_texture();
  SDL_Renderer *sdl_renderer();
  SDL_Window *sdl_window();

private:
  std::unique_ptr<pixel_t[]> pixels;

  WindowSettings const window_settings;

  SDL_Texture *texture;
  SDL_Renderer *renderer;
  SDL_Window *window;
};

} // namespace pr
