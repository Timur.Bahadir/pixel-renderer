#include "PixelRenderer/SDL_PixelRenderer.hpp"

namespace pr {

SDL_PixelRenderer::SDL_PixelRenderer(WindowSettings const &ws)
    : window_settings{ws} {
  window = SDL_CreateWindow(ws.window_name.c_str(), SDL_WINDOWPOS_CENTERED,
                            SDL_WINDOWPOS_CENTERED, ws.window_width,
                            ws.window_height, SDL_WINDOW_OPENGL);
  if (window == NULL) {
    SDL_Log("Unable to create window: %s", SDL_GetError());
    throw std::bad_alloc{};
  }

  renderer = SDL_CreateRenderer(window, -1, SDL_RENDERER_ACCELERATED);
  if (renderer == NULL) {
    SDL_Log("Unable to create renderer: %s", SDL_GetError());
    throw std::bad_alloc{};
  }

  texture = SDL_CreateTexture(renderer, SDL_PIXELFORMAT_ARGB8888,
                              SDL_TEXTUREACCESS_STATIC, ws.texture_width,
                              ws.texture_height);
  if (texture == NULL) {
    SDL_Log("Unable to create texture: %s", SDL_GetError());
    throw std::bad_alloc{};
  }

  pixels = std::make_unique<pixel_t[]>(ws.texture_width * ws.texture_height);
  memset(pixels.get(), 255,
         ws.texture_width * ws.texture_height * sizeof(pixel_t));
}

SDL_PixelRenderer::~SDL_PixelRenderer() {
  SDL_DestroyTexture(texture);
  SDL_DestroyRenderer(renderer);
  SDL_DestroyWindow(window);
}

void SDL_PixelRenderer::change_pixel(size_t const x, size_t const y,
                                     pixel_t const px) {
  pixels[y * window_settings.texture_width + x] = px;
}

void SDL_PixelRenderer::draw() {
  SDL_UpdateTexture(texture, NULL, pixels.get(),
                    window_settings.texture_width * sizeof(uint32_t));
  SDL_RenderCopy(renderer, texture, NULL, NULL);
  SDL_RenderPresent(renderer);
}

SDL_Texture *SDL_PixelRenderer::sdl_texture() { return texture; }
SDL_Renderer *SDL_PixelRenderer::sdl_renderer() { return renderer; }
SDL_Window *SDL_PixelRenderer::sdl_window() { return window; }

} // namespace pr
