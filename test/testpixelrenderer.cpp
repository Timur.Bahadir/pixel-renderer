#include "PixelRenderer/SDL_PixelRenderer.hpp"

#include <SDL2/SDL.h>

#include <memory>

#ifdef __WIN32
#include "windows.h"
#endif

constexpr uint16_t Window_Width{1280}, Window_Height{720};
constexpr uint16_t Texture_Width{640}, Texture_Height{360};

#ifdef __WIN32
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE prevInstance, LPSTR lpCmdLine,
                   int cmdShow)
#else
int main(int argc, char **argv)
#endif // __WIN32
{
  SDL_Init(SDL_INIT_EVERYTHING);

  pr::WindowSettings ws{"Renderer test", Window_Width, Window_Height,
                        Texture_Width, Texture_Height};

  std::unique_ptr<pr::PixelRenderer> pixrender;
  try {
    pixrender = std::make_unique<pr::SDL_PixelRenderer>(ws);
  } catch (std::bad_alloc const &e) {
    SDL_Quit();
    return EXIT_FAILURE;
  }

  bool running{true};
  uint8_t iterations = 10;

  SDL_Event event;
  while (running) {
    while (SDL_PollEvent(&event)) {
      switch (event.type) {
      case SDL_QUIT:
        running = false;
        break;
      }
    }

    running = --iterations > 0;
  }

  SDL_Quit();
  return EXIT_SUCCESS;
}
